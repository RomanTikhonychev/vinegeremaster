//
//  TRTextDecoder.h
//  VinegereMaster
//
//  Created by Рома on 2/26/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TRTextDecoder : NSObject

+ (NSString *)guessSypherKeyForText:(NSString *)text
                      usingAlphabet:(NSString *)alphabet
                          keyLenght:(NSInteger)keyLenght
            popularInAlphabetLetter:(NSString *)popularLetter;


+ (NSString *)decryptText:(NSString *)text withKey:(NSString *)key usingAlphabet:(NSString *)alphabet;

@end
