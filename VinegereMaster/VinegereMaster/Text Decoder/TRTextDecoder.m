//
//  TRTextDecoder.m
//  VinegereMaster
//
//  Created by Рома on 2/26/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import "TRTextDecoder.h"
#import "TRIndexManager.h"
#import "TRFrequencyAnalyzer.h"

#define mod %

static const NSInteger singleLetter = 1;


@implementation TRTextDecoder

+ (NSString *)guessSypherKeyForText:(NSString *)text
                     usingAlphabet:(NSString *)alphabet
                         keyLenght:(NSInteger)keyLenght
           popularInAlphabetLetter:(NSString *)popularLetter
{

    NSMutableArray *alphabetLetters = [NSMutableArray new];
    NSInteger alphabetLenght = [alphabet length];
    
    NSInteger popularLetterIndex = -1;
    
    for (NSInteger i = 0; i < alphabetLenght; i++) {
        
        NSString *letter = [alphabet substringWithRange:NSMakeRange(i, 1)];
        [alphabetLetters addObject:letter];
        
        if ([letter isEqualToString:popularLetter]) {
            popularLetterIndex = i;
        }
    }
    
    
    
    NSArray *blockedText = [TRIndexManager spreadText:text inToBlocksWithPeriod:keyLenght];
    
    NSMutableString *purposeKey = [[NSMutableString alloc]initWithString:@""];
    
    for (NSInteger k = 0; k < keyLenght; k++) {
        
        NSString  *currentYText = blockedText[k];
        
        NSInteger mostPopularLetterInEncrypdedTextIndex = [TRFrequencyAnalyzer findMostPopularLetterInText:currentYText wihtAlphabet:alphabet];
        
        NSInteger tempIndex = mostPopularLetterInEncrypdedTextIndex - popularLetterIndex;
        
        if (tempIndex < 0) {
            tempIndex = tempIndex + alphabetLenght;
        }
        
        NSInteger letterIndexAtKPosition = tempIndex mod alphabetLenght;
        
        [purposeKey appendString:alphabetLetters[letterIndexAtKPosition]];
    }
    
    
    return purposeKey;
}


+ (NSString *)decryptText:(NSString *)text withKey:(NSString *)key usingAlphabet:(NSString *)alphabet
{
    
    NSMutableArray *alphabetLetters = [NSMutableArray new];
    
    NSInteger       alphabetLenght  = [alphabet length];
    NSInteger       keyLenght       = [key length];
    
    for (NSInteger i = 0; i < alphabetLenght; i++) {
        
        NSString *letter = [alphabet substringWithRange:NSMakeRange(i, 1)];
        [alphabetLetters addObject:letter];
    }
    
    NSMutableString *decryptedText = [[NSMutableString alloc] initWithString:@""];

    for (NSInteger index = 0; index < [text length]; index++) {
        
        NSString *textSymbol    = [text substringWithRange:NSMakeRange(index, singleLetter)];
        NSString *keySymbol     = [key  substringWithRange:NSMakeRange(index mod keyLenght, singleLetter)];
        
        NSInteger textSymbolAlphabetNumber = -1;
        NSInteger keySymbolAlphabetNumber = -1;
        
        for (int k = 0; k < alphabetLenght; k++) {
            
            NSString *testAlphabetLetter = [alphabetLetters objectAtIndex:k];
            
            if ([textSymbol isEqualToString:testAlphabetLetter]) {
                textSymbolAlphabetNumber = k;
            }
            
            if ([keySymbol isEqualToString:testAlphabetLetter]) {
                keySymbolAlphabetNumber = k;
            }
            
        }
        
        
        NSInteger decryptedSymolAlphabetIndex = (textSymbolAlphabetNumber - keySymbolAlphabetNumber) mod alphabetLenght;
        
        if (decryptedSymolAlphabetIndex < 0) {
            
            decryptedSymolAlphabetIndex = decryptedSymolAlphabetIndex + alphabetLenght;
        }
        
        NSString *decryptedSymbol = [alphabetLetters objectAtIndex:decryptedSymolAlphabetIndex];
        
        
        [decryptedText appendString:decryptedSymbol];
    }
    
    return decryptedText;
}

@end





