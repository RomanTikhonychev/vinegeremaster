//
//  TREncryptingManager.h
//  VinegereMaster
//
//  Created by Рома on 2/23/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TREncryptingManager : NSObject

+ (NSString *)encryptText:(NSString *)text withKey:(NSString *)key wihtAlphapet:(NSString *)alphabet;

@end
