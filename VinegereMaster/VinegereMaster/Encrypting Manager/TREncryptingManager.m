//
//  TREncryptingManager.m
//  VinegereMaster
//
//  Created by Рома on 2/23/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import "TREncryptingManager.h"

#import <math.h>
#include<locale.h>


#define mod %

static const NSInteger singleLetter = 1;


@implementation TREncryptingManager

+ (NSString *)encryptText:(NSString *)text withKey:(NSString *)key wihtAlphapet:(NSString *)alphabet
{
    NSMutableArray *alphabetLetters = [NSMutableArray new];
    
    int alphabetLenght  = (int)[alphabet length];
    int keyLenght       = (int)[key length];
    
    for (int i = 0; i < alphabetLenght; i++) {
        
        NSString *letter = [alphabet substringWithRange:NSMakeRange(i, singleLetter)];
        [alphabetLetters addObject:letter];
    }
    
    
    NSMutableString *encryptedText = [[NSMutableString alloc] initWithString:@""];

    for (long long int i = 0; i < [text length]; i++) {
        
        NSString *textSymbol    = [text substringWithRange:NSMakeRange(i, singleLetter)];
        NSString *keySymbol     = [key  substringWithRange:NSMakeRange(i mod keyLenght, singleLetter)];

        NSInteger textSymbolAlphabetNumber = -1;
        NSInteger keySymbolAlphabetNumber = -1;
        
        for (int k = 0; k < alphabetLenght; k++) {
            
            NSString *testAlphabetLetter = [alphabetLetters objectAtIndex:k];
            
            if ([textSymbol isEqualToString:testAlphabetLetter]) {
                textSymbolAlphabetNumber = k;
            }
            
            if ([keySymbol isEqualToString:testAlphabetLetter]) {
                keySymbolAlphabetNumber = k;
            }
            
        }
    
        NSInteger encryptedSymbolNumber = (textSymbolAlphabetNumber + keySymbolAlphabetNumber) mod alphabetLenght;
        NSString *encryptedSymbol = [alphabetLetters objectAtIndex: (int)encryptedSymbolNumber];
        
        [encryptedText appendString:encryptedSymbol];
        
    }

    return encryptedText;
}

@end




