//
//  AppDelegate.m
//  VinegereMaster
//
//  Created by Рома on 2/26/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import "AppDelegate.h"

#import "TRFrequencyAnalyzer.h"
#import "TREncryptingManager.h"
#import "TRTextCleaner.h"
#import "TRIndexManager.h"
#import "TRTextDecoder.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    
    //    NSString* path = [[NSBundle mainBundle] pathForResource:@"1"
    //                                                     ofType:@"txt"];
    //
    //    NSError *error = [[NSError alloc] init];
    //    NSString* content = [NSString stringWithContentsOfFile:path
    //                                                  encoding:NSUnicodeStringEncoding
    //                                                     error:&error];
    
    
    
    //    NSDictionary *result = [TRFrequencyAnalyzer calculateLetterFrequencyForText:content wihtAlphapet:@"абвгдеёжзийклмнопрстуфхцчшщъыьэюя"];
    //    NSArray *leters = result[@"alphabetLetters"];
    //    NSArray *frequency = result[@"alphabetLettersFrequency"];
    //
    //    NSInteger countLettersInText = 0;
    //    for (int i = 0; i < [leters count]; i++) {
    //
    //        NSLog(@"letter = %@ frequency = %@\n", leters[i],frequency[i]);
    //        countLettersInText = countLettersInText + [frequency[i] integerValue];
    //
    //    }
    //    NSLog(@"Letters in text: %li", (long)countLettersInText);
    
    
    
    
    
    //    NSDictionary *result = [TRFrequencyAnalyzer calculateBigrammaFrequencyForText:@"абвабвабвабваа" wihtAlphapet:@"абв"];
    //    NSLog(@"Frequency %@",result);
    //
    
    
    
    
//    NSString* path = [[NSBundle mainBundle] pathForResource:@"LevTolstoi_AnnaKarenina"
//                                                     ofType:@"txt"];
//
//    NSError *error = [[NSError alloc] init];
//    NSString* content = [NSString stringWithContentsOfFile:path
//                                                  encoding:NSUnicodeStringEncoding
//                                                     error:&error];
//
//    NSString *cleaneContent = [TRTextCleaner cleaneText:content andLeaveNextCharacters:RussianAlphabet];
//
//    
//    CFTimeInterval startTime = CACurrentMediaTime();
//
//    CGFloat result2 = [TRFrequencyAnalyzer calculateLetterEntropyForText:cleaneContent
//                                                            wihtAlphapet:RussianAlphabet];
//
//    CFTimeInterval elapsedTime = CACurrentMediaTime() - startTime;
//
//    CGFloat time = (CGFloat)elapsedTime;
//    NSLog(@"Entropy: %f Time: %f",result2, time);
//    
//    NSLog(@"stop here");
    
    
    
//    NSString* path = [[NSBundle mainBundle] pathForResource:@"Lev_Tolstoy_voyna_I_Mir"
//                                                     ofType:@"txt"];
//
//    NSError *error = [[NSError alloc] init];
//    NSString* content = [NSString stringWithContentsOfFile:path
//                                                  encoding:NSUnicodeStringEncoding
//                                                     error:&error];
//
//    NSString *cleaneContent = [TRTextCleaner cleaneText:content andLeaveNextCharacters:RussianAlphabetEE];
//
//    CFTimeInterval startTime = CACurrentMediaTime();
//
//    CGFloat result = [TRFrequencyAnalyzer calculateBigrammaEntropyForText:cleaneContent
//                                                             wihtAlphapet:RussianAlphabetEE];
//
//    CFTimeInterval elapsedTime = CACurrentMediaTime() - startTime;
//
//    CGFloat time = (CGFloat)elapsedTime;
//    NSLog(@"Entropy: %f Time: %f",result, time);
//    
//    NSLog(@"stop here");
    
    
    //    NSString *result = [TREncryptingManager encryptText:@"абвгабвг" withKey:@"аб" wihtAlphapet:@"абвг"];
    //    NSLog(@"Cypher: %@",result);
    
    
// Ekzyuperi_Malenky_Prints
//    NSString* path = [[NSBundle mainBundle] pathForResource:@"3"
//                      
//                                                     ofType:@"txt"];
//    
//    NSError *error = [[NSError alloc] init];
//    NSString* content = [NSString stringWithContentsOfFile:path
//                         
//                                                  encoding:NSUnicodeStringEncoding
//                         
//                                                     error:&error];
//    
//    NSString *cleaneContent = [TRTextCleaner cleaneText:content andLeaveNextCharacters:RussianAlphabet];
//    
//    CFTimeInterval startTime = CACurrentMediaTime();
//    
//    NSString *encryptedText = [TREncryptingManager encryptText:cleaneContent withKey:@"криптографиянаука" wihtAlphapet:RussianAlphabet];
//    
//    CFTimeInterval elapsedTime = CACurrentMediaTime() - startTime;
//    CGFloat time = (CGFloat)elapsedTime;
//    
//    NSLog(@"Encrypted text: %@ Time: %f",encryptedText, time);
//    
//    
//    //try to write in file
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents directory
//    
//    
//    NSError *error2;
//    BOOL succeed = [encryptedText writeToFile:[documentsDirectory stringByAppendingPathComponent:@"myfile.txt"]
//                    
//                                   atomically:YES encoding:NSUnicodeStringEncoding error:&error2];
//    
//    if (!succeed) {
//        
//    }
    
    
//            NSString* path = [[NSBundle mainBundle] pathForResource:@"4"
//                                                             ofType:@"txt"];
//    
//            NSError *error = [[NSError alloc] init];
//            NSString* content = [NSString stringWithContentsOfFile:path
//                                                          encoding:NSUnicodeStringEncoding
//                                                             error:&error];
//    
//            NSString *cleaneContent = [TRTextCleaner cleaneText:content andLeaveNextCharacters:nil];
//    
//            CFTimeInterval startTime = CACurrentMediaTime();
//    
//           NSArray *blockedText = [TRIndexManager spreadText:cleaneContent inToBlocksWithPeriod:3];
//    
//            CFTimeInterval elapsedTime = CACurrentMediaTime() - startTime;
//            CGFloat time = (CGFloat)elapsedTime;
//            NSLog(@"Encrypted text: %@ Time: %f",blockedText, time);
    
    

    
    //index for russian
//    NSString* path = [[NSBundle mainBundle] pathForResource:@"1"
//                                                         ofType:@"txt"];
//
//    NSError *error = [[NSError alloc] init];
//    NSString* content = [NSString stringWithContentsOfFile:path
//                                                      encoding:NSUnicodeStringEncoding
//                                                         error:&error];
//    
//    NSString *cleaneText = [TRTextCleaner cleaneText:content andLeaveNextCharacters:nil];
//    
//    CGFloat indexOfAccordance = [TRIndexManager countIndexOfAccordanceForBlockOfText:cleaneText withAplhabet:RussianAlphabet];
//    
//    NSLog(@"index of Accordance for Russian = %f", indexOfAccordance);
    
    //index of Accordance for Russian = 0.056418
    
    
   // index for cypher ключ
//        NSString* path = [[NSBundle mainBundle] pathForResource:@"5"
//                                                             ofType:@"txt"];
//    
//        NSError *error = [[NSError alloc] init];
//        NSString* content = [NSString stringWithContentsOfFile:path
//                                                          encoding:NSUnicodeStringEncoding
//                                                             error:&error];
//    
//        NSString *cleaneText = [TRTextCleaner cleaneText:content andLeaveNextCharacters:nil];
//    
//    
//    
//    
//    NSArray *indexesForEncryptedText = [TRIndexManager calculateIndexsOfAccordancesForText:cleaneText usingAlphabet:RussianAlphabet withSMALLPeriod: 5];
//    
//        NSLog(@"indexes of Accordance = %@", indexesForEncryptedText);
    
    
//    2 = (
//         "0.043338",
//         "0.037002"
//         )
    
//   3 = (
//       "0.036511",
//       "0.035837",
//       "0.033326"
//       )
    
//    4 = (               ///висновок, ключ довжини 4 =)
//         "0.062375",
//         "0.051579",
//         "0.051313",
//         "0.049451"
//         )
    
//    5 = (
//         "0.037724",
//         "0.034505",
//         "0.033663",
//         "0.034505",
//         "0.032401"
//         )
    
    
    
    
    
    
    
     //calculateStatisticMatchForText //key lenght = 14
//        NSString* path = [[NSBundle mainBundle] pathForResource:@"6"
//                                                             ofType:@"txt"];
//
//        NSError *error = [[NSError alloc] init];
//        NSString* content = [NSString stringWithContentsOfFile:path
//                                                          encoding:NSUnicodeStringEncoding
//                                                             error:&error];
//
//        NSString *cleaneText = [TRTextCleaner cleaneText:content andLeaveNextCharacters:nil];
//
//
//
//    for (NSInteger index = 6; index < 31; index++) {
//        
//        CGFloat statisticMatch = [TRIndexManager calculateStatisticMatchForText:cleaneText usingAlphabet:RussianAlphabet withBIGPeriod:index];
//        
//        NSLog(@"statisticMatch for period = %ld  match = %f\n", (long)index, statisticMatch);
//        
//    }
    
    
    
    
//    //find most popular letter in open text
//    NSString* path = [[NSBundle mainBundle] pathForResource:@"3"
//                                                     ofType:@"txt"];
//    
//    NSError *error = [[NSError alloc] init];
//    NSString* content = [NSString stringWithContentsOfFile:path
//                                                  encoding:NSUnicodeStringEncoding
//                                                     error:&error];
//    
//    NSString *cleaneText = [TRTextCleaner cleaneText:content andLeaveNextCharacters:nil];
//    
//    NSInteger popularSymbolIndex = [TRFrequencyAnalyzer findMostPopularLetterInText:cleaneText wihtAlphabet:RussianAlphabet];
//    
    
    
    //decode text
    
//    NSString *alphabet = RussianAlphabet;
//
//    int alphabetLenght = (int)[alphabet length];
//
//    for (int i = 0; i < alphabetLenght; i++) {
//
//        NSString *letter = [alphabet substringWithRange:NSMakeRange(i, 1)];
//
//    NSString* path = [[NSBundle mainBundle] pathForResource:@"6"
//                                                         ofType:@"txt"];
//
//    NSError *error = [[NSError alloc] init];
//    NSString* content = [NSString stringWithContentsOfFile:path
//                                                      encoding:NSUnicodeStringEncoding
//                                                         error:&error];
//
//    NSString *cleaneText = [TRTextCleaner cleaneText:content andLeaveNextCharacters:nil];
//
//    NSString *guessKey = [TRTextDecoder guessSypherKeyForText:cleaneText usingAlphabet:RussianAlphabet keyLenght:14 popularInAlphabetLetter:letter];
//    
//    NSLog(@"Letter: %@ key: %@", letter, guessKey);
//        
//    }
    
#warning encrypt laba cypher text (index of Accordance for Russian = 0.056418)
    
    
 //index for cypher ключ
    
//        NSString* path = [[NSBundle mainBundle] pathForResource:@"cypher"
//                                                             ofType:@"txt"];
//
//        NSError *error = [[NSError alloc] init];
//        NSString* content = [NSString stringWithContentsOfFile:path
//                                                          encoding:NSUnicodeStringEncoding
//                                                             error:&error];
//
//        NSString *cleaneText = [TRTextCleaner cleaneText:content andLeaveNextCharacters:RussianAlphabetEE];
//
//
//
//    for (int i = 2; i < 6; i++) {
//        
//        NSArray *indexesForEncryptedText = [TRIndexManager calculateIndexsOfAccordancesForText:cleaneText usingAlphabet:RussianAlphabet withSMALLPeriod: i];
//        
//        NSLog(@"keylenght = %i indexes of Accordance = %@", i, indexesForEncryptedText);
//    }
    
    //значения не близки к 0.056418 посчитаем D
    
    
    
////calculateStatisticMatchForText
//        NSString* path = [[NSBundle mainBundle] pathForResource:@"cypher"
//                                                             ofType:@"txt"];
//
//        NSError *error = [[NSError alloc] init];
//        NSString* content = [NSString stringWithContentsOfFile:path
//                                                          encoding:NSUnicodeStringEncoding
//                                                             error:&error];
//
//        NSString *cleaneText = [TRTextCleaner cleaneText:content andLeaveNextCharacters:RussianAlphabetEE];
//
//
//
//    for (NSInteger index = 6; index < 31; index++) {
//
//        CGFloat statisticMatch = [TRIndexManager calculateStatisticMatchForText:cleaneText usingAlphabet:RussianAlphabet withBIGPeriod:index];
//
//        NSLog(@"keyLen = %ld  match = %f\n", (long)index, statisticMatch);
//        
//    }
    
    //  при keyLen = 15 наибольшое количество повторений - ключ имеет длину 15)
    

//    NSString *alphabet = RussianAlphabetEE;
//    
//    int alphabetLenght = (int)[alphabet length];
//    
//    for (int i = 0; i < alphabetLenght; i++) {
//        
//        NSString *letter = [alphabet substringWithRange:NSMakeRange(i, 1)];
//        
//        
//        
//        //decode key
//        NSString* path = [[NSBundle mainBundle] pathForResource:@"cypher"
//                                                         ofType:@"txt"];
//        
//        NSError *error = [[NSError alloc] init];
//        NSString* content = [NSString stringWithContentsOfFile:path
//                                                      encoding:NSUnicodeStringEncoding
//                                                         error:&error];
//        
//        NSString *cleaneText = [TRTextCleaner cleaneText:content andLeaveNextCharacters:RussianAlphabetEE];
//        
//        NSString *guessKey = [TRTextDecoder guessSypherKeyForText:cleaneText usingAlphabet:RussianAlphabetEE keyLenght:15 popularInAlphabetLetter:letter];
//        
//        NSLog(@"letter: %@ key: %@\n\n", letter, guessKey);
//        
//    }
    
   // NSInteger letterAlphabetIndex = [TRFrequencyAnalyzer findMostPopularLetterInText:@"aaaabbaabbaaaa" wihtAlphabet:@"ab"];

    
  //decrypting
    
   // NSString* path = [[NSBundle mainBundle] pathForResource:@"cypher"
   //                                                 ofType:@"txt"];

    // NSError *error = [[NSError alloc] init];
//NSString* content = [NSString stringWithContentsOfFile:path
//                                                  encoding:NSUnicodeStringEncoding
//                                                     error:&error];
    
//    NSString *decryptedText = [TRTextDecoder decryptText:content withKey:@"абсолютныйигрок" usingAlphabet:RussianAlphabetEE];
//    NSLog(@"Decrypted text: %@", decryptedText);
    
#warning Find all varianst key len:
    
   
//    for (int i = 1; i < 21; i++) {
//    
//        NSLog(@"\n\nTEXT Number :%i\n",i);
//        
//        NSString *file = [[NSString alloc] initWithFormat: @"%i", i];
//        NSString *path = [[NSBundle mainBundle] pathForResource:file ofType:@"txt"];
//        
//        NSError  *error = [[NSError alloc] init];
//        NSString *content = [NSString stringWithContentsOfFile:path
//                                                      encoding:NSUnicodeStringEncoding
//                                                         error:&error];
//        
//
//        
//        NSLog(@"\n\nNEW TEXT\n");
//
//        for (int k = 2; k < 6; k++) {
//    
//            NSArray *indexesForEncryptedText = [TRIndexManager calculateIndexsOfAccordancesForText:content usingAlphabet:RussianAlphabetEE withSMALLPeriod: k];
//    
//            NSLog(@"TextNumber %i keylenght = %i indexes of Accordance = %@", i, k, indexesForEncryptedText);
//        }
//
//        for (NSInteger index = 6; index < 31; index++) {
//            
//            CGFloat statisticMatch = [TRIndexManager calculateStatisticMatchForText:content usingAlphabet:RussianAlphabetEE withBIGPeriod:index];
//            
//            NSLog(@"TextNumber %i keyLen = %ld  match = %f\n",i ,(long)index, statisticMatch);
//            
//        }
//    }
//    
//    NSLog(@"\n\nEND");
    
    
#warning try to guess key
    

//    NSArray *keyLenghtArray = @[@12, @14, @14, @13, @16, @17, @15, @20, @17, @15, @17, @14, @17, @19, @14, @21, @15, @15, @16, @14];
//    
//    
//    for (int i = 1; i < 21; i++) {
//
//        NSLog(@"\n\n\nTEXT NUMBER %i\n", i);
//        NSArray *letters = @[@"о", @"а", @"е", @"и", @"н", @"т"];
//        
//        
//        for (NSString *letter in letters) {
//            
//            
//            NSString *file = [[NSString alloc] initWithFormat: @"%i", i];
//            NSString *path = [[NSBundle mainBundle] pathForResource:file ofType:@"txt"];
//            
//            NSError *error = [[NSError alloc] init];
//            NSString* content = [NSString stringWithContentsOfFile:path
//                                                          encoding:NSUnicodeStringEncoding
//                                                             error:&error];
//            
//            NSInteger keyLenght = [[keyLenghtArray objectAtIndex:i-1] integerValue];
//            
//            NSString *guessKey = [TRTextDecoder guessSypherKeyForText:content usingAlphabet:RussianAlphabetEE keyLenght:keyLenght popularInAlphabetLetter:letter];
//            
//            NSLog(@"Text: %i letter: %@ key: %@\n", i, letter, guessKey);
//            
//
//        }
//    }
//
//    NSLog(@"\nEND");
    
    
#warning test unicode
//    
//    NSString *alphabetString = RussianAlphabet;
//    
//    for (int i = 0; i < [alphabetString length]; i++) {
//        
//        int temp = (int)[alphabetString characterAtIndex:i];
//        
//        NSLog(@"i = %i, char number = %i char %@", i, temp, [alphabetString substringWithRange:NSMakeRange(i, 1)]);
//    }
    
    
#warning 4-5 task
    
//    NSArray *keysArray = @[@"мп",
//                           @"мал",
//                           @"мапр",
//                           @"принц",
//                           @"цветыслабые",
//                           @"маленькийпринц",
//                           @"барашеквкоробке",
//                           @"вседорогиведутклюдям"];
//    
//    for (int b = 0; b < [keysArray count]; b++) {
//        
//        NSString *key = [keysArray objectAtIndex:b];
//        
//        NSString* path = [[NSBundle mainBundle] pathForResource:@"Ekzyuperi_Malenky_Prints"
//                                                         ofType:@"txt"];
//        NSError *error = [[NSError alloc] init];
//        NSString* content = [NSString stringWithContentsOfFile:path
//                                                      encoding:NSUnicodeStringEncoding
//                                                         error:&error];
//        
//        NSString *cleaneContent = [TRTextCleaner cleaneText:content andLeaveNextCharacters:RussianAlphabet];
//        
//        CFTimeInterval startTime = CACurrentMediaTime();
//        
//        NSString *encryptedText = [TREncryptingManager encryptText:cleaneContent withKey:key wihtAlphapet:RussianAlphabet];
//        
//        CFTimeInterval elapsedTime = CACurrentMediaTime() - startTime;
//        CGFloat time = (CGFloat)elapsedTime;
//        
//        NSLog(@"Encrypted text: %@ Time: %f",encryptedText, time);
//        
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents directory
//        
//        NSError *error2;
//        NSString *pathComponent = [[NSString alloc] initWithFormat:@"EMP_Key_%@.txt",key];
//        BOOL succeed = [encryptedText writeToFile:[documentsDirectory stringByAppendingPathComponent:pathComponent]
//                        
//                                       atomically:YES encoding:NSUnicodeStringEncoding error:&error2];
//    
//        if (!succeed) {
//            
//        }
//    }
//    
//    NSLog(@"Cypher Done!");
    
    
    
    
//    NSArray *keysArray = @[@"мп",
//                           @"мал",
//                           @"мапр",
//                           @"принц",
//                           @"цветыслабые",
//                           @"маленькийпринц",
//                           @"барашеквкоробке",
//                           @"вседорогиведутклюдям"];
//    
//    NSString* path = [[NSBundle mainBundle] pathForResource:@"Ekzyuperi_Malenky_Prints"
//                                                     ofType:@"txt"];
//    
//    NSError *error = [[NSError alloc] init];
//    NSString* content = [NSString stringWithContentsOfFile:path
//                                                  encoding:NSUnicodeStringEncoding
//                                                     error:&error];
//    
//    CGFloat indexesForEncryptedText = [TRIndexManager countIndexOfAccordanceForBlockOfText:content withAplhabet:RussianAlphabet];
//    
//    NSLog(@"   input text indexes of Accordance = %f", indexesForEncryptedText);
//
//    for (NSString *key in keysArray) {
//        
//        
//        NSString *fileName = [[NSString alloc] initWithFormat:@"EMP_Key_%@",key];
//        
//        NSString* path = [[NSBundle mainBundle] pathForResource:fileName
//                                                         ofType:@"txt"];
//        
//        NSError *error = [[NSError alloc] init];
//        NSString* content = [NSString stringWithContentsOfFile:path
//                                                      encoding:NSUnicodeStringEncoding
//                                                         error:&error];
//            
//        CGFloat indexesForEncryptedText = [TRIndexManager countIndexOfAccordanceForBlockOfText:content withAplhabet:RussianAlphabet];
//        
//            NSLog(@"keylenght = %lu indexes of Accordance = %f", (unsigned long)[key length], indexesForEncryptedText);
//        
//        
//    }
    
    
    
    
//    for (NSInteger index = 6; index < 31; index++) {
//
//        CGFloat statisticMatch = [TRIndexManager calculateStatisticMatchForText:cleaneText usingAlphabet:RussianAlphabet withBIGPeriod:index];
//
//        NSLog(@"keyLen = %ld  match = %f\n", (long)index, statisticMatch);
//        
//    }
    
    
#pragma mark - Test 
    
    /*
     
     При великих r можна застосувати метод, що використовує такий факт: в шифртексті на відстанях, що кратні періоду, однакові символи будуть зустрічатись частіше, ніж на будь-яких інших. 
     
     
     */
    
    
#pragma mark - Get key Lenght
    
    NSString* path = [[NSBundle mainBundle] pathForResource:@"cypher"
                                                     ofType:@"txt"];
    
    NSError *error = [[NSError alloc] init];
    NSString* content = [NSString stringWithContentsOfFile:path
                                                  encoding:NSUnicodeStringEncoding
                                                     error:&error];
    
    NSString *cleaneText = [TRTextCleaner cleaneText:content andLeaveNextCharacters:RussianAlphabetEE];
    
    
    
    for (NSInteger index = 6; index < 31; index++) {
        
        CGFloat statisticMatch = [TRIndexManager calculateStatisticMatchForText:cleaneText usingAlphabet:RussianAlphabet withBIGPeriod:index];
        
        NSLog(@"keyLen = %ld  match = %f\n", (long)index, statisticMatch);
        
    }
    
#pragma mark - try to guess Cipher Key
    
    
    NSString *alphabet = RussianAlphabetEE;
    
    int alphabetLenght = (int)[alphabet length];
    
    for (int i = 0; i < alphabetLenght; i++) {
        
        NSString *letter = [alphabet substringWithRange:NSMakeRange(i, 1)];
        
        
        
        //decode key
        NSString* path = [[NSBundle mainBundle] pathForResource:@"cypher"
                                                         ofType:@"txt"];
        
        NSError *error = [[NSError alloc] init];
        NSString* content = [NSString stringWithContentsOfFile:path
                                                      encoding:NSUnicodeStringEncoding
                                                         error:&error];
        
        NSString *cleaneText = [TRTextCleaner cleaneText:content andLeaveNextCharacters:RussianAlphabetEE];
        
        NSString *guessKey = [TRTextDecoder guessSypherKeyForText:cleaneText usingAlphabet:RussianAlphabetEE keyLenght:15 popularInAlphabetLetter:letter];
        
        NSLog(@"letter: %@ key: %@\n\n", letter, guessKey);
        
    }
    
#pragma mark - Decode Text With Key
    
    
    NSString *decryptedText = [TRTextDecoder decryptText:content withKey:@"абсолютныйигрок" usingAlphabet:RussianAlphabetEE];
    NSLog(@"Decrypted text: %@", decryptedText);
    
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end









