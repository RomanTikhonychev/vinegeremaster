//
//  TRTextCleaner.h
//  VigenereMaster
//
//  Created by Рома on 2/25/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TRTextCleaner : NSObject

+ (NSString *)cleaneText:(NSString *)dirtyText andLeaveNextCharacters:(NSString *)leaveCharacters;

@end
