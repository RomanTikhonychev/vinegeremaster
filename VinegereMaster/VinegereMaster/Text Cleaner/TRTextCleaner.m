//
//  TRTextCleaner.m
//  VigenereMaster
//
//  Created by Рома on 2/25/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import "TRTextCleaner.h"

@implementation TRTextCleaner

+ (NSString *)cleaneText:(NSString *)dirtyText andLeaveNextCharacters:(NSString *)leaveCharacters
{
    NSString *lowerCaseText = [dirtyText lowercaseString];
    
    NSMutableCharacterSet *charactersToKeep = [NSMutableCharacterSet new];
    
    if(leaveCharacters) {
        [charactersToKeep addCharactersInString:leaveCharacters];
    }
    
    NSCharacterSet *charactersToRemove = [charactersToKeep invertedSet];

    NSString *outputText = [[lowerCaseText componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];
    
    return outputText;
}

@end
